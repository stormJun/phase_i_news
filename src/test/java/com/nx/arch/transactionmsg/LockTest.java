package com.nx.arch.transactionmsg;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nx.arch.addon.lock.NxLock;
import com.nx.arch.addon.lock.NxLockClient;
import com.nx.arch.addon.lock.exception.LockException;

public class LockTest {
    private static final Logger log = LoggerFactory.getLogger(LockTest.class);
    
    NxLockClient lockClient;
    
    String key = "hello_lock";
    
    String[] configStr;
    
    public LockTest(String... etcd) {
        configStr = etcd;
        System.out.println(Arrays.toString(etcd));
        try {
            lockClient = NxLock.factory().etcdSolution().connectionString(etcd).clusterName("hello_transmsg").build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void testLock() {
        // for(int i=0;i<3;i++){
        HelloRun run = new HelloRun();
        Thread thread = new Thread(run);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        // }
        /*
         * try { Thread.sleep(2000000); } catch (InterruptedException e) { e.printStackTrace(); }
         */
    }
    
    public static void main(String[] args) {
        String[] etcd = {"http://etcd1.lock.nxinc.com:2379", "http://etcd2.lock.nxinc.com:2379", "http://etcd3.lock.nxinc.com:2379", "http://etcd4.lock.nxinc.com:2379", "http://etcd5.lock.nxinc.com:2379"};
        // ApplicationContext context = new FileSystemXmlApplicationContext("src/test/resources/application.xml");
        LockTest lock = new LockTest(etcd);
        lock.testLock();
        System.out.println("main close");
    }
    
    class HelloRun implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < 200; i++) {
                NxLock lock = null;
                try {
                    // url = "hello_/lock://helloworld"
                    lock = lockClient.newLock("hello.org_lock_helloworld");
                    if (lock.acquire(10)) {
                        log.info("Thread {} acquire {}", Thread.currentThread(), i);
                        Thread.sleep(3);
                        log.info("Thread {} sleep end {}", Thread.currentThread(), i);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    try {
                        lock.release();
                    } catch (LockException e) {
                        log.info("lockException {} {}", e.getMessage(), i);
                    }
                }
            }
            try {
                lockClient.close();
                System.out.println("close end");
            } catch (LockException e) {
                e.printStackTrace();
            }
        }
    }
    
}
